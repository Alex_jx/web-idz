<?php
require_once 'db.php';
require_once 'vendor/autoload.php';
Twig_Autoloader::register();

$loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'].'/templates');
$twig = new Twig_Environment($loader);

$data = [];
$headers = [];
if(isset($_POST['action'])){
    if($_POST['action'] == 'ward'){
        $request = $link->query( "SELECT w.name 
                                    FROM nurse n
                                    LEFT JOIN nurse_ward nw ON nw.fid_nurse = n.id_nurse
                                    LEFT JOIN ward w ON nw.fid_ward = w.id_ward
                                    WHERE n.id_nurse = ".$_POST['nurse']);

        $headers = [];
        array_push($headers, 'Ward Name');
        $data = [];
        $i=0;
        while ($row = mysqli_fetch_assoc($request)){
            $data['name'][$i] = $row['name'];
            $i++;
        }
    }

    if($_POST['action'] == 'department'){
        $request = $link->query( "SELECT name 
                                    FROM nurse 
                                    WHERE department = ".$_POST['department']);

        $headers = [];
        array_push($headers, 'Department Name');
        $data = [];
        $i=0;
        while ($row = mysqli_fetch_assoc($request)){
            $data['name'][$i] = $row['name'];
            $i++;
        }
    }

    if($_POST['action'] == 'shift'){
        $request = $link->query( "SELECT n.name as nurse_name, w.name as ward_name, n.date
                                    FROM nurse n
                                    LEFT JOIN nurse_ward nw ON nw.fid_nurse = n.id_nurse
                                    LEFT JOIN ward w ON nw.fid_ward = w.id_ward
                                    WHERE n.shift = '".$_POST['shift']."'");


        $headers = [];
        array_push($headers, 'Nurse name');
        array_push($headers, 'Ward');
        array_push($headers, 'Date');
        $data = [];
        $i=0;
        while ($row = mysqli_fetch_assoc($request)){
            $data['nurse_name'][$i] = $row['nurse_name'];
            $data['ward_name'][$i] = $row['ward_name'];
            $data['date'][$i] = $row['date'];
            $i++;
        }
    }
}


echo $twig->render('index.twig', ['data' => $data, 'headers'=>$headers, 'nurses'=>$nurses, 'department'=>$department, 'shift'=>$shift, 'wards'=>$wards]);
